Jarmod2Coremod - a coremod to load jarmods

1. Install jarmod2coremod in "coremods"
2. Place your jarmods in "jarmods" in "CLIENT" or "SERVER" subfolders, as appropriate
3. Start Minecraft and see if your jarmods load

Important Notes:

* Rename the jarmods to control their loader, they will be sorted alphabetically before loading
* If you have trouble, test by installing jarmods into the jar instead

Compilation:

* Install [Maven](http://maven.apache.org/)
* `mvn initialize -P -built`
* `mvn package`

Downloads:

* Latest builds available at Buildhive: [![Build Status](https://buildhive.cloudbees.com/job/agaricusb/job/Jarmod2Coremod/badge/icon)](https://buildhive.cloudbees.com/job/agaricusb/job/Jarmod2Coremod/)

